package com.xzixi.websocket.interceptablewebsocket.util;

import lombok.Data;

/**
 * @author 薛凌康
 */
@Data
public class MessageFromClient {

    /**
     * 消息类型
     */
    private String type;
    /**
     * 订阅id
     */
    private String subId;
    /**
     * 主题
     */
    private String destination;
    /**
     * 内容长度
     */
    private Integer contentLength;
    /**
     * 内容
     */
    private String content;

}
