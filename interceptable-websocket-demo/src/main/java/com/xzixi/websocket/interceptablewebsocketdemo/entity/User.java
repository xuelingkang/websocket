package com.xzixi.websocket.interceptablewebsocketdemo.entity;

import lombok.Data;

import java.util.List;

/**
 * @author 薛凌康
 */
@Data
public class User {

    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 用户权限
     */
    private List<Resource> resources;

}
