package com.xzixi.websocket.interceptablewebsocket.util;

import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import java.util.Objects;

/**
 * @author 薛凌康
 */
@Component
public class MessageMatcher {

    private PathMatcher matcher = new AntPathMatcher();

    /**
     * 验证客户端消息的主题和类型
     * @param pattern 与message的destination属性匹配的模板，可以是通配符
     * @param message 客户端消息
     * @param type 消息类型
     * @return {@code true} 验证成功，{@code false} 验证失败
     */
    public boolean matches(String pattern, String type, MessageFromClient message) {
        if (!Objects.equals(type, message.getType())) {
            return false;
        }
        String destination = message.getDestination();
        if (pattern==null) {
            pattern = "";
        }
        if (destination==null) {
            destination = "";
        }
        return matcher.match(pattern, destination);
    }

}
