package com.xzixi.websocket.interceptablewebsocket.interceptor;

import com.xzixi.websocket.interceptablewebsocket.extension.InterceptableStompSubProtocolHandler;
import com.xzixi.websocket.interceptablewebsocket.util.MessageFromClient;
import org.springframework.messaging.MessageChannel;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.messaging.StompSubProtocolHandler;

import java.security.Principal;

/**
 * websocket消息拦截器，
 * 拦截客户端发来的消息
 * @author 薛凌康
 */
public interface FromClientInterceptor {

    /**
     * 前置处理
     * @see InterceptableStompSubProtocolHandler#handleMessageFromClient(WebSocketSession, WebSocketMessage, MessageChannel)
     * @param session websocket session
     * @param principal 认证信息
     * @param message websocket消息
     * @param outputChannel websocket消息通道
     * @param handler stomp协议控制器
     * @return true 执行后续操作，false 取消后续操作
     */
    default boolean preHandle(WebSocketSession session, Principal principal, MessageFromClient message, MessageChannel outputChannel, StompSubProtocolHandler handler) {
        return true;
    }

    /**
     * 后置处理
     * @see InterceptableStompSubProtocolHandler#handleMessageFromClient(WebSocketSession, WebSocketMessage, MessageChannel)
     * @param session websocket session
     * @param principal 认证信息
     * @param message websocket消息
     * @param outputChannel websocket消息通道
     * @param handler stomp协议控制器
     */
    default void postHandle(WebSocketSession session, Principal principal, MessageFromClient message, MessageChannel outputChannel, StompSubProtocolHandler handler) {

    }

}
