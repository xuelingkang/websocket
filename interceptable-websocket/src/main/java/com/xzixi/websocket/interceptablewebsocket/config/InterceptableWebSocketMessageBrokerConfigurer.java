package com.xzixi.websocket.interceptablewebsocket.config;

import com.xzixi.websocket.interceptablewebsocket.extension.InterceptableWebMvcStompEndpointRegistry;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * 重载{@link #registerStompEndpoints(StompEndpointRegistry)}
 * 将{@link StompEndpointRegistry}向下造型为{@link InterceptableWebMvcStompEndpointRegistry}
 * 子类直接重写{@link #registerStompEndpoints(InterceptableWebMvcStompEndpointRegistry)}即可，方便增加消息拦截器
 * 示例：
 * <pre>
 * &#064;Configuration
 * &#064;EnableInterceptableWebSocketMessageBroker
 * public class WebSocketSecurityConfig implements InterceptableWebSocketMessageBrokerConfigurer {
 * 	   &#064;Override
 * 	   protected void registerStompEndpoints(InterceptableWebMvcStompEndpointRegistry registry) {
 *         registry.addFromClientInterceptor(exampleFromClientInterceptor)
 *                 .addToClientInterceptor(exampleToClientInterceptor);
 * 	   }
 * }
 * </pre>
 * @author 薛凌康
 */
public interface InterceptableWebSocketMessageBrokerConfigurer extends WebSocketMessageBrokerConfigurer {

    /**
     * 注册stomp映射
     * @param registry stomp映射器
     */
    @Override
    default void registerStompEndpoints(StompEndpointRegistry registry) {
        if (registry instanceof InterceptableWebMvcStompEndpointRegistry) {
            registerStompEndpoints((InterceptableWebMvcStompEndpointRegistry) registry);
        }
    }

    /**
     * 注册stomp映射
     * @param registry 自定义的stomp映射器
     */
    default void registerStompEndpoints(InterceptableWebMvcStompEndpointRegistry registry) {
    }

}
