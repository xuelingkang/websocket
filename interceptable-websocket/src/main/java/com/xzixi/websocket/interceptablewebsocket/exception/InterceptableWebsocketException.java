package com.xzixi.websocket.interceptablewebsocket.exception;

/**
 * @author 薛凌康
 */
public class InterceptableWebsocketException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InterceptableWebsocketException() {
    }

    public InterceptableWebsocketException(String message) {
        super(message);
    }

    public InterceptableWebsocketException(String message, Throwable cause) {
        super(message, cause);
    }

    public InterceptableWebsocketException(Throwable cause) {
        super(cause);
    }

    public InterceptableWebsocketException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
