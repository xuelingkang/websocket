package com.xzixi.websocket.interceptablewebsocketdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 薛凌康
 */
@SpringBootApplication
public class InterceptableWebsocketDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterceptableWebsocketDemoApplication.class, args);
    }

}
