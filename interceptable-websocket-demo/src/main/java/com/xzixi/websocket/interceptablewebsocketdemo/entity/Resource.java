package com.xzixi.websocket.interceptablewebsocketdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 薛凌康
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Resource {

    /**
     * 协议，http ws
     */
    private String protocol;
    /**
     * 路径，/* /abc /abc/*
     */
    private String pattern;
    /**
     * 类型，GET POST PUT PATCH DELETE SUBSCRIBE
     */
    private String type;

}
