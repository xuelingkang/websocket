package com.xzixi.websocket.interceptablewebsocketdemo.service;

import com.xzixi.websocket.interceptablewebsocketdemo.entity.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 薛凌康
 */
@Service
public class ResourceService {

    /**
     * 模拟查询用户权限，本案例只做webSocket消息授权决策，http请求忽略
     * @param username 用户名
     * @return 用户权限
     */
    public List<Resource> findResourceByUsername(String username) {
        List<Resource> resources = new ArrayList<>();
        if ("admin".equals(username)) {
            // admin用户权限
            resources.add(new Resource("ws", "/topic/broadcast", "SUBSCRIBE"));
            resources.add(new Resource("ws", "/user/topic/chat", "SUBSCRIBE"));
        } else if ("user".equals(username)) {
            // user用户权限
            resources.add(new Resource("ws", "/topic/broadcast", "SUBSCRIBE"));
            resources.add(new Resource("ws", "/user/topic/chat", "SUBSCRIBE"));
        } else if ("guest".equals(username)) {
            // guest用户权限
            resources.add(new Resource("ws", "/topic/broadcast", "SUBSCRIBE"));
        }
        return resources;
    }

}
