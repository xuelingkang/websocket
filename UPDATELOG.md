- 2019-09-25 v2.0

  - 去掉spring-security依赖

  - 重构`interceptable-websocket`项目和案例

- 2019-07-23 v1.0

  - 实现stomp消息的细粒度拦截
