package com.xzixi.websocket.interceptablewebsocket.annotation;

import com.xzixi.websocket.interceptablewebsocket.extension.InterceptableWebSocketMessageBrokerConfiguration;
import com.xzixi.websocket.interceptablewebsocket.util.MessageMatcher;
import com.xzixi.websocket.interceptablewebsocket.util.NotifyUtil;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 要使用消息拦截的功能，必须在启动类或配置类上标注此注解
 * @author 薛凌康
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({InterceptableWebSocketMessageBrokerConfiguration.class, MessageMatcher.class, NotifyUtil.class})
public @interface EnableInterceptableWebSocketMessageBroker {

}
